Name: ccaudio2
Description: GNU Telephonic audio library for use with ucommon
Version: ${VERSION}
Requires: ucommon >= 6.0.0
Libs:  -lccaudio2 ${PACKAGE_LIBS}
Cflags: ${PACKAGE_FLAGS}

