# Copyright (c) 2014 David Sugar, Tycho Softworks.
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

Name: ccaudio2
Summary: C++ class framework for telephonic audio applications
Version: @VERSION@
Release: 0%{?dist}
License: LGPLv3+
URL: http://www.gnutelephony.org/index.php/GNU_ccAudio2
Group: System Environment/Libraries
Source0: http://dev.gnutelephony.org/dist/tarballs/ccaudio2-%{version}.tar.gz
BuildRequires: ucommon-devel >= 6.0.0
BuildRequires: gsm-devel
BuildRequires: speex-devel
BuildRequires: pkgconfig(ucommon)
BuildRequires: cmake

%description
The GNU telephonic audio library covers a range of functionality including
tone generation and detection, streaming and transcoding of encoded audio
frames, accessing of audio files on disk, phrasebook management, and audio
utilities.

%package bin
Requires: %{name}%{?_isa} = %{version}-%{release}
Group: Applications/System
Summary: GNU ccaudio support applications

%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: ucommon-devel%{?_isa} >= 6.0.0
Group: Development/Libraries
Summary: Headers for building ccaudio applications

%description bin
This is a collection of command line tools that use various aspects of the
ccaudio2 library.  This includes command line tools for telephonic processing
of audio files.

%description devel
This package provides header and support files for building applications
that use GNU ccAudio.

%prep
%setup -q
%build
%cmake \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DSYSCONFDIR=%{_sysconfdir} \
      -DINSTALL_MANDIR=%{_mandir} \
      -DINSTALL_INCLUDEDIR=%{_includedir} \
	  -DINSTALL_DATADIR=%{_datadir} \
      -DINSTALL_BINDIR=%{_bindir} \
      -DINSTALL_LIBDIR=%{_libdir} \
      .

%{__make} %{?_smp_mflags}

%install
%{__make} DESTDIR=%{buildroot} INSTALL="install -p" install

%files
%doc AUTHORS README COPYING COPYING.LESSER NEWS SUPPORT ChangeLog
%{_libdir}/*.so.*
%config(noreplace) %{_sysconfdir}/tones.conf

%files bin
%{_bindir}/audiotool
%{_bindir}/tonetool
%{_mandir}/man1/audiotool.*
%{_mandir}/man1/tonetool.*

%files devel
%{_libdir}/*.so
%{_includedir}/ccaudio2.h
%{_libdir}/pkgconfig/*.pc

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog

